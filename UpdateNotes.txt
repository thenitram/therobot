March 23, 2017
- Imported Robot assets.
- Implemented move, jump, punch scripts.
- Hooked-up move, jump and punch animations.

March 24, 2017
- Implemented Outline shader.
- Imported temporary textures.
- Updated move and jump scripts.
- Added Traps and Platforms.

March 25, 2017
- Added new temporary textures.
- Added mainmenu, win and gameover scenes.
- Implemented Collectibles.
- Implemented breakable walls.
- Completed level1 setup.
- Updated move, jump and punch scripts to make it plug&play.

March 25, 2017
- Added audio assets.
- Implemented fly mechanics.
- Implemented respawn mechanics.
- Added life hud.

