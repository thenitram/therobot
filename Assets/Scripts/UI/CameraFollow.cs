﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	[SerializeField] Transform target;
	[SerializeField] Vector3 offset;
	float smoothFollow = 0.1f;
	Vector3 position;

	void Start () {
		Debug.Assert(target != null, "Unassigned Target field in the Inspector");
		offset = transform.position - target.transform.position;

	}
	

	void LateUpdate () {
		if(target == null){
			return;
		}

		Vector3 position = target.transform.position + offset;
		position.y = transform.position.y;
		transform.position = Vector3.Lerp(transform.position, position, smoothFollow);
	}
}
