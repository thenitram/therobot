﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITextMessage : MonoBehaviour {

	[SerializeField] Text message;
	[SerializeField] int duration;

	IEnumerator enumerator;

	void Start(){
		SetMessage("A/D to move left and right");
		enumerator = Fade();
	}

	public void SetMessage(string text){
		this.message.text = text;
		SetDefaultVisibility();

		if(enumerator != null){
			StopCoroutine(enumerator);
		}
		enumerator = Fade();
		StartCoroutine(enumerator);
	}

	void SetDefaultVisibility(){
		Color color = message.color;
		color.a = 1;
		message.color = color;
	}

	IEnumerator Fade(){
		float timeLimit = duration;
		Color color = message.color;
		float toSubtract = 1f/duration*0.1f;
		while(timeLimit > 0.1f){
			timeLimit -= 0.1f;
			color.a -= toSubtract;
			message.color = color;
			yield return new WaitForSeconds(0.1f);
		}
		message.text = "";
		SetDefaultVisibility();
		yield return null;
	}
}
