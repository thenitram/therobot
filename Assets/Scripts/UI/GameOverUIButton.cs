﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Common;

public class GameOverUIButton : UIButton, IPointerClickHandler  {

	#region IPointerClickHandler
	void IPointerClickHandler.OnPointerClick (PointerEventData eventData) {
		SceneManager.LoadScene(Constants.MAIN_MENU_SCENE);
	}
	#endregion {
}

