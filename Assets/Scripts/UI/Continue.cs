﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Common;

public class Continue : UIButton, IPointerClickHandler  {

	#region IPointerClickHandler
	void IPointerClickHandler.OnPointerClick (PointerEventData eventData) {
		SceneManager.LoadScene(Constants.GAME_SCENE);
	}
	#endregion {
}

