﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeHud : MonoBehaviour {

	[SerializeField] Text lifeCount;
	[SerializeField] Stats clientStat;

	void Start () {
		Debug.Assert(lifeCount != null, "Missing LifeCount Text component.");
		Debug.Assert(clientStat != null, "Missing Stats component.");
	}
	
	void Update () {
		lifeCount.text = "x"+clientStat.GetLife();
	}
}
