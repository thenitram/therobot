﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common{
	
	public class UIButton : MonoBehaviour, IPointerClickHandler {

		[SerializeField] int buttonID;

		#region IPointerClickHandler
		public void OnPointerClick (PointerEventData eventData) {
			throw new System.NotImplementedException("Cannot use this class directly. Use a concrete class.");
		}
		#endregion {

		#region Setter Getter
		protected int GetID(){
			return buttonID;
		}
		#endregion
	}
}
