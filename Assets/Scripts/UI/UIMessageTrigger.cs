﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMessageTrigger : MonoBehaviour {

	[SerializeField] UITextMessage textMessage;
	[SerializeField] string message;

	void OnTriggerEnter(Collider other){
		if(textMessage != null){
			textMessage.SetMessage(message);
			Destroy(gameObject, 0.5f);
		}
	}
}
