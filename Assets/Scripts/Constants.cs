﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants {

	public static string GAME_SCENE = "Game";
	public static string MAIN_MENU_SCENE = "MainMenu";
	public static string GAME_OVER = "GameOver";
	public static string LEVEL_COMPLETE = "LevelComplete";
}
