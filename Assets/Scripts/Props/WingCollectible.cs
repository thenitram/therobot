﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingCollectible : Collectible {

	public override void Run (Transform client) {
		Jump comp = client.GetComponent<Jump>();
		if(comp == null){
			client.gameObject.AddComponent<Jump>();
			Destroy(gameObject);			
		}
		else{
			comp.SetCanFly(true);
			Destroy(gameObject);	
		}
	}

	void OnTriggerEnter(Collider other){
		Run(other.transform);
	}
}
