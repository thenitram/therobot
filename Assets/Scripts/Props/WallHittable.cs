﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallHittable : Hittable {

	[SerializeField] GameObject poofParticle;

	public override void GetHit () {
		poofParticle = Instantiate(poofParticle, transform.position, Quaternion.identity);
		Destroy(gameObject);
	}
}
