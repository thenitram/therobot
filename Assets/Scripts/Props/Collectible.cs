﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Collectible : MonoBehaviour {

	//TODO: Call this from client instead on the collectible
	public virtual void Run(Transform client){

	}
}
