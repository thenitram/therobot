﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

public class FinishLevel : MonoBehaviour {

	[SerializeField] GameObject poofParticle;

	void OnTriggerEnter(Collider other){
		//TODO: cleanup
		other.gameObject.SetActive(false);
		poofParticle = Instantiate(poofParticle, transform.position, Quaternion.identity);
		GameController.GetInstance().SetGameState(GameState.LEVEL_COMPLETE);
	}
}
