﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : Collectible {

	public override void Run (Transform client) {
		Destroy(gameObject);
	}

	void OnTriggerEnter(Collider other){
		Run(other.transform);
	}
}
