﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hittable : MonoBehaviour {

	public virtual void GetHit(){
		throw new System.NotImplementedException("Cannot use this class directly. Use concrete class.");
	}
}
