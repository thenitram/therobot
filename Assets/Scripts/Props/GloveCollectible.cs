﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GloveCollectible : Collectible {

	public override void Run (Transform client) {
		Punch comp = client.GetComponent<Punch>();
		if(comp == null){
			client.gameObject.AddComponent<Punch>();
			Destroy(gameObject);
		}
	}

	void OnTriggerEnter(Collider other){
		Run(other.transform);
	}
}
