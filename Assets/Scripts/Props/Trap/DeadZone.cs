﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZone : ActiveTrap {

//	void Update(){
//		if(GetIsActive() == true){
//			Destroy(gameObject);
//		}
//	}

	void OnCollisionEnter(Collision other){

		//Checks if this obj collided with trappable obj.
		Trappable comp = other.transform.GetComponent<Trappable>();
		if(comp != null){
			comp.SetIsTrapped(true);
		}
	}
}
