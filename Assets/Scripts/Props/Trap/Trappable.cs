﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trappable : MonoBehaviour {

	bool isTrapped;

	#region Setter Getter
	public void SetIsTrapped(bool isTrapped){
		this.isTrapped = isTrapped;
	}

	public bool GetIsTrapped(){
		return isTrapped;
	}
	#endregion
}
