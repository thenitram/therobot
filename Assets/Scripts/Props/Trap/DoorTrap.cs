﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrap : Trap {

	[SerializeField] GameObject poofParticle;

	void Update(){
		if(GetIsActive() == true){
			Destroy(gameObject);
		}
	}

	void OnTriggerEnter(Collider other){
		//Checks if this obj collided with trappable obj.
		Trappable comp = other.transform.GetComponent<Trappable>();
		if(comp != null){
			//Do nothing since this is just a passive trap.
			poofParticle = Instantiate(poofParticle, transform.position, Quaternion.identity);
			Destroy(gameObject);
		}
	}
}
