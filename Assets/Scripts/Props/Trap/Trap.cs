﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Trap : MonoBehaviour {

	//Variable to check if this trap is active.
	bool isActive;

	#region Setter Getter
	protected void SetIsActive(bool isActive){
		this.isActive = isActive;
	}

	protected bool GetIsActive(){
		return isActive;
	}
	#endregion
}
