﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShoeCollectible : Collectible {

	public override void Run (Transform client) {
		Jump comp = client.GetComponent<Jump>();
		if(comp == null){
			client.gameObject.AddComponent<Jump>();
			Destroy(gameObject);			
		}
	}

	void OnTriggerEnter(Collider other){
		Run(other.transform);
	}
}
