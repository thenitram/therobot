﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Stats))]
public class RobotTrappable : Trappable {

	[SerializeField] Transform[] partToDisable;
	[SerializeField] float blinkTime = 3;
	Stats stats;
	RespawnClient respawn;

	void Update () {
		if(GetIsTrapped() == true && GetStats().GetIsInvinsible() == false){
			GetStats().Hit();
			GetStats().SetIsInvinsible(true);
			SetIsTrapped(false);
			GetRoboRespawn().SetPosition();
			AddBlink();
		}
	}

	void AddBlink(){
		Blink comp = gameObject.AddComponent<Blink>();
		comp.Initialize(partToDisable,blinkTime,BlinkCallback);
	}

	#region Events messages and callbacks
	/// <summary>
	/// Callback when the blink effect takes off.
	/// </summary>
	void BlinkCallback(){
		GetStats().SetIsInvinsible(false);
	}
	#endregion

	#region Lazy loading
	Stats GetStats(){
		stats = stats ?? GetComponent<Stats>();
		Debug.Assert(stats != null, "Missing Stats component on " + gameObject.name);
		return stats;	
	}

	RespawnClient GetRoboRespawn(){
		respawn = respawn ?? GetComponent<RespawnClient>();
		Debug.Assert(stats != null, "Missing RoboRespawn component on " + gameObject.name);
		return respawn;	
	}
	#endregion
}
