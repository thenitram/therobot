﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {

	[SerializeField] bool isInvinsible = false;
	[SerializeField] int life = 3;
	bool isDead = false;

	#region Setter Getter
	public void SetIsInvinsible(bool isInvinsible){
		this.isInvinsible = isInvinsible;
	}

	public void SetIsDead(bool isDead){
		this.isDead = isDead;
		if(this.isDead){
			life = 0;
		}
	}

	public bool GetIsDead(){
		return isDead;
	}

	public bool GetIsInvinsible(){
		return isInvinsible;
	}

	public void Hit(){
		if(GetIsInvinsible()){
			return;
		}

		life -= 1;
		if(life <= 0){
			SetIsDead(true);
		}
	}

	public int GetLife(){
		return life;
	}
	#endregion
}
