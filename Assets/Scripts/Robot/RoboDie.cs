﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoboDie : MonoBehaviour {

	[SerializeField] GameObject dieParticle;

	void Start () {
		this.enabled = false;
	}

	public void Run(){
		dieParticle = Instantiate(dieParticle, transform.position, Quaternion.identity);
	}
}
