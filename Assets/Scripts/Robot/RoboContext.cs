﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;
using UnityEngine.UI;

[RequireComponent(typeof(RoboAnimationController))]
[RequireComponent(typeof(InputManager))]
[RequireComponent(typeof(RoboDie))]
[RequireComponent(typeof(Stats))]

public class RoboContext : MonoBehaviour {

	RoboAnimationController animationController;
	Movement movement;
	Punch punch;
	Jump jump;
	RobotTrappable trappable;
	InputManager inputHandler;
	RoboDie roboDie;
	Stats stats;
	[SerializeField] Transform punchPoint;

	//TODO: Remove this when global messaging is implemented.
	[SerializeField] UITextMessage textMessage;

	//Flags to prevent multiple excutions of instructions in fixedUpdate.
	bool didJump = false;
	bool didPunch = false;
	bool isDead = false;

	#region Unity Life cycle
	void Start(){
		inputHandler = GetInputHandler();
		GetMovement().Initialize(inputHandler);
	}
	
	void FixedUpdate () {
		//Checks if robo is dead.
		if(GetStats().GetIsDead() && isDead == false){
			isDead = true;
			GetRoboDie().Run();
			GameController.GetInstance().SetGameState(GameState.LEVEL_FAILED);
			Destroy(gameObject);
			return;
		}

		UpdateMovement();

		//Disable punching if currently jumping.
		if(IsJumping() && GetPunch() != null){
			GetPunch().SetIsPunching(false);
		}
		//Disable jumping if currently punching.
		else if(IsPunching() && GetJump() != null){
			GetJump().SetIsJumping(false);
			GetMovement().SetToIdle();
		}
//		else if(GetJump() != null && GetJump().GetIsJumping() == false){
//			if(GetPunch() != null && GetPunch().GetIsPunching() == false){
//				GetInputHandler().SetIsInputAllowed(true);
//			}
//		}
	}
	#endregion

	#region Actions
	/// <summary>
	/// Updates the movement animation of the animation controller based on the movement data.
	/// </summary>
	void UpdateMovement(){
		if(GetMovement().GetIsWalking() == true){
			GetAnimationController().SetToWalking();
		}
		else{
			GetAnimationController().SetToIdle();
		}
	}

	/// <summary>
	/// Determines whether this instance is punching.
	/// </summary>
	/// <returns><c>true</c> if this instance is punching; otherwise, <c>false</c>.</returns>
	bool IsPunching(){
		if(GetPunch() == null) {
			return false;
		}

		if(GetPunch().GetIsPunching() == true && didPunch == false){
			didPunch = true;
			GetAnimationController().SetToPunching();
			GetInputHandler().SetIsInputAllowed(false);
		}
		else if(GetPunch().GetIsPunching() == false && didPunch == true){
			didPunch = false;
			inputHandler.SetIsInputAllowed(true);

			//Shoot a raycast for any target that accepts punch.
			Debug.DrawLine(transform.position, punchPoint.transform.position, Color.white, 10);

			RaycastHit[] hits = Physics.RaycastAll(transform.position, punchPoint.transform.position - transform.position, Vector3.Distance(transform.position, punchPoint.transform.position));
			RaycastHit hit;
			for(int x=0; x<hits.Length; x++){
				hit = hits[x];
				if(hit.transform != null){
					Hittable comp = hit.transform.GetComponent<Hittable>();
					if(comp != null){
						comp.GetHit();
					}
					break;
				}
			}
		}

		return didPunch;
	}

	/// <summary>
	/// Checks for jump instructions and updates animation controller.
	/// </summary>
	bool IsJumping(){
		if(GetJump() == null) {
			return false;
		}

		if((GetJump().GetIsJumping() == true && didJump == false) || (GetJump().GetIsFlying() == true)){
			didJump = true;
			GetAnimationController().SetToJumping();
		}
		else if(GetJump().GetIsJumping() == false && didJump == true){
			didJump = false;
		}

		return didJump;
	}
	#endregion

	#region LazyLoading
	/// <summary>
	/// Gets the current RoboAnimationController component.
	/// </summary>
	/// <returns>The animator.</returns>
	RoboAnimationController GetAnimationController(){
		animationController = animationController ?? GetComponent<RoboAnimationController>();
		Debug.Assert(animationController != null, "Missing RoboAnimationController component on " + gameObject.name);
		return animationController;
	}

	/// <summary>
	/// Gets the current Movement component.
	/// </summary>
	/// <returns>The movement.</returns>
	Movement GetMovement(){
		movement = movement ?? GetComponent<Movement>();
		Debug.Assert(movement != null, "Missing Movement component on " + gameObject.name);
		return movement;
	}
		
	/// <summary>
	/// Gets the current Punch component.
	/// </summary>
	/// <returns>The punch.</returns>
	Punch GetPunch(){
		if(punch == null){
			punch = GetComponent<Punch>();
			if(punch != null){
				punch.Initialize(GetInputHandler());
				textMessage.SetMessage("Press \"E\" to Punch");
			}
		}
		return punch;
	}

	Jump GetJump(){
		if(jump == null){
			jump = GetComponent<Jump>();
			if(jump != null){
				jump.Initialize(GetInputHandler());
				textMessage.SetMessage("Press \"Space\" to Jump");
			}
		}
		return jump;
	}

	RobotTrappable GetTrappable(){
		trappable = trappable ?? GetComponent<RobotTrappable>();
		Debug.Assert(trappable != null, "Missing Punch component on " + gameObject.name);
		return trappable;	
	}

	InputManager GetInputHandler(){
		inputHandler = inputHandler ?? GetComponent<InputManager>();
		Debug.Assert(inputHandler != null, "Missing InputHandler component on " + gameObject.name);
		return inputHandler;	
	}
		
	RoboDie GetRoboDie(){
		roboDie = roboDie ?? GetComponent<RoboDie>();
		Debug.Assert(inputHandler != null, "Missing InputHandler component on " + gameObject.name);
		return roboDie;
	}

	Stats GetStats(){
		stats = stats ?? GetComponent<Stats>();
		Debug.Assert(inputHandler != null, "Missing Stats component on " + gameObject.name);
		return stats;
	}
	#endregion
}
