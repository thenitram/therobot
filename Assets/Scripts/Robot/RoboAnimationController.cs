﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoboAnimationController : MonoBehaviour {

	Animator animator;

	public void SetToJumping(){
		GetAnimator().SetTrigger("jump");
	}

	public void SetToPunching(){
		GetAnimator().SetTrigger("punch");
	}

	public void SetToWalking(){
		GetAnimator().SetInteger("state",1);
	}

	public void SetToIdle(){
		GetAnimator().SetInteger("state",0);
	}

	#region LazyLoading
	/// <summary>
	/// Gets the current animator.
	/// </summary>
	/// <returns>The animator.</returns>
	Animator GetAnimator(){
		animator = animator ?? GetComponent<Animator>();
		Debug.Assert(animator != null, "Missing Animator component on " + gameObject.name);
		return animator;
	}
	#endregion
}
