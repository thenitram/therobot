﻿using UnityEngine;
using System.Collections;
using Common;

public class GameController : MonoBehaviour {

	private static GameController instance;

	void Awake () {
		instance = this;
	}

	//TODO: Move this method
	#region
	void GameOver(){
		SceneManager.LoadScene(Constants.GAME_OVER);
	}

	void LevelComplete(){
		SceneManager.LoadScene(Constants.LEVEL_COMPLETE);
	}
	#endregion

	public void SetGameState(GameState state){
		if(state == GameState.LEVEL_FAILED){
			Invoke("GameOver", 2f);
		}
		else if(state == GameState.LEVEL_COMPLETE){
			Invoke("LevelComplete", 3f);
		}
	}

	public static GameController GetInstance(){
		if (instance == null){
			GameObject obj = new GameObject();
			obj.name = "GameController";
			instance = obj.AddComponent<GameController>();
			obj.AddComponent<DoNotDestroy>();
		}
		return instance;
	}
}

public enum GameState{
	NONE,
	WILL_START,
	STARTING,
	STARTED,
	RUNNING,
	PAUSE,
	RESUMING,
	TIMER_END, // State when count down reaches to 0.
	LEVEL_COMPLETE,// State when the user successfully passed the ROUND.
	LEVEL_FAILED, // State when the user failed the ROUND.
}

