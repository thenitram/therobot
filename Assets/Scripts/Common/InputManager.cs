﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Common{
	
	public class InputManager : MonoBehaviour {

		InputData data = new InputData();
//		List<InputDelegate> listDelegate = new List<InputDelegate>();
//		public delegate void InputDelegate(InputData data);
		bool isInputAllowed = true;
		#region Unity lifecyle
		void Update () {
			float x = Input.GetAxis("Horizontal");
			float y = Input.GetAxis("Vertical");
			data.xAxis = x;
			data.yAxis = y;

			#region Click and Drag
			if(Input.GetMouseButtonDown(0)){
				//Get first position.
				data.pos1 = Input.mousePosition;
				data.button1IsReleased = false;
				data.button1IsPressed = true;
			}

			else if(Input.GetMouseButtonUp(0)){
				//Get second position.
				data.pos2 = Input.mousePosition;
				data.button1IsPressed = false;
				data.button1IsReleased = true;

				data.Reset();
			}

			//Jump
			if(Input.GetKeyDown(KeyCode.Space)){
				data.space = ButtonState.ON_DOWN;
			}
			else if(Input.GetKeyUp(KeyCode.Space)){
				data.space = ButtonState.ON_RELEASE;
			}
			else{
				data.space = ButtonState.ON_IDLE;
			}

			//Punch
			if(Input.GetKeyDown(KeyCode.E)){
				data.e = ButtonState.ON_DOWN;
			}
			else if(Input.GetKeyUp(KeyCode.E)){
				data.e = ButtonState.ON_RELEASE;
			}
			else{
				data.e = ButtonState.ON_IDLE;
			}
			#endregion

			//BroadcastInput(data);
		}
		#endregion

		#region Input Broadcast methods
//		void Register(InputDelegate method){
//			listDelegate.Add(method);
//		}
//
//		void Unregister(InputDelegate method){
//			listDelegate.Remove(method);
//			method = null;
//		}
//
//		void BroadcastInput(InputData data){
//			InputDelegate method;
//			for(int x=0; x<listDelegate.Count; x++){
//				method = listDelegate[x];
//				if(method != null){
//					method(data);
//				}
//			}
//		}
		#endregion

		#region Setter Getter
		public InputData GetInputData(){
			return data;
		}

		public void SetIsInputAllowed(bool isInputAllowed){
			this.isInputAllowed = isInputAllowed;
		}

		public bool IsInputAllowed(){
			return isInputAllowed;
		}
		#endregion
	}

	//TODO: Clean this up. 32417
	public class InputData{
		//Gesture positions.
		public Vector3 pos1 = Vector3.zero;
		public Vector3 pos2 = Vector3.zero;
		public Vector3 direction = Vector3.zero;

		public bool button1IsPressed = false;
		public bool button1IsReleased = false;

		public float xAxis = 0f;
		public float yAxis = 0f;
		public float zAxis = 0f;

		public ButtonState space;
		public ButtonState e;

		public void Reset(){
			pos1 = Vector3.zero;
			pos2 = Vector3.zero;
			direction = Vector3.zero;
			button1IsPressed = false;
			button1IsReleased = false;
		}
	}

	public enum ButtonState{
		ON_IDLE,
		ON_PRESS,
		ON_DOWN,
		ON_RELEASE,
	}
}

