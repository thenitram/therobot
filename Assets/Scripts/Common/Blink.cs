﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Blink : MonoBehaviour {

	[SerializeField] float duration = 3;
	[SerializeField] Transform[] objectsToBlink;
	Action callback;

	void Start(){
		StartCoroutine(Run());
	}
	
	public void Initialize(Transform[] objects, float duration, Action callback = null){
		this.objectsToBlink = objects;
		this.duration = duration;
		this.callback = callback;
	}

	IEnumerator Run(){
		float timeLimit = duration;
		GameObject obj;
		while(timeLimit > 0){
			timeLimit -= 0.1f;
			for(int x=0; x<objectsToBlink.Length; x++){
				obj = objectsToBlink[x].gameObject; 
				obj.SetActive(!obj.activeSelf);
			}
			yield return new WaitForSeconds(0.1f);
		}

		for(int x=0; x<objectsToBlink.Length; x++){
			obj = objectsToBlink[x].gameObject; 
			obj.SetActive(true);
		}

		//run callback if any
		if(callback != null){
			callback();
			callback = null;
		}
		Destroy(this, 0.5f);
		yield return null;
	}
}
