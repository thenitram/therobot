﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class RespawnPoint : MonoBehaviour {

	void OnTriggerEnter(Collider other){
		//Checks if this obj collided with respawnable obj.
		RespawnClient comp = other.transform.GetComponent<RespawnClient>();
		if(comp != null){
			comp.SetRespawnPoint(transform);
		}
	}
}
