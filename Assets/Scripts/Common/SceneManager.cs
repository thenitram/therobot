﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace Common{
	
	public class SceneManager : MonoBehaviour {

		public static void LoadScene (string sceneName, bool isAdditive = false) {
			UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName, (isAdditive) ?LoadSceneMode.Additive :LoadSceneMode.Single);
		}

		public static void LoadScene (int sceneID, bool isAdditive = false) {
			UnityEngine.SceneManagement.SceneManager.LoadScene(sceneID, (isAdditive) ?LoadSceneMode.Additive :LoadSceneMode.Single);
		}
	}
}