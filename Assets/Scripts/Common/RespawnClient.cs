﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnClient : MonoBehaviour {

	[SerializeField] Transform spawnPoint;

	public void SetPosition(){
		if(spawnPoint != null){
			transform.position = spawnPoint.position;
		}
	}

	public void SetRespawnPoint(Transform position){
		spawnPoint = position;
	}
}
