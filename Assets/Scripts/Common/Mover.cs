﻿using UnityEngine;
using System.Collections;

[System.Serializable]

public class Mover : MonoBehaviour {

	//private
	float xPos;
	float yPos;
	bool isMax = false;

	enum Movement{
		stationary,
		vertical,
		horizontal
	}

	[SerializeField] Movement movement;
	[SerializeField] int maxAmount;
	[SerializeField] float step;

	void Start () {
		xPos = transform.position.x;
		yPos = transform.position.y;
	}

	void Update () {
		//set max value
		switch(movement){
		case Movement.vertical:
			if(transform.position.y >= yPos+maxAmount)
				isMax = true;
			else if(transform.position.y <= yPos)
				isMax = false;
			break;
		case Movement.horizontal:
			if(transform.position.x >= xPos+maxAmount)
				isMax = true;
			else if(transform.position.x <= xPos)
				isMax = false;
			break;
		default: //stationary
			break;
		}

		//move the platform
		Vector3 tempPosition = transform.position;
		switch(movement){
		case Movement.vertical:
			tempPosition.y = (!isMax)?tempPosition.y+step*Time.deltaTime:tempPosition.y-step*Time.deltaTime;
			break;
		case Movement.horizontal:
			tempPosition.x = (!isMax)?tempPosition.x+step*Time.deltaTime:tempPosition.x-step*Time.deltaTime;
			break;
		default: //stationary
			break;
		}
		transform.position = tempPosition;
	}
}
