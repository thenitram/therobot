﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

[RequireComponent(typeof(InputManager))]
public class Punch : MonoBehaviour {

	bool isPunching = false;
	InputManager inputHandler;

	//For injection
	public void Initialize(InputManager input){
		this.inputHandler = input;
	}

	void Update () {
		//TODO: Fix this.
		if(GetInputHandler() != null && GetInputHandler().IsInputAllowed()){
			if(GetInputHandler().GetInputData().e == ButtonState.ON_DOWN){
				SetIsPunching(true);
			}
		}
	}

	#region Setter Getter
	public void SetIsPunching(bool isPunching){
		this.isPunching = isPunching;
	}

	public bool GetIsPunching(){
		return isPunching;
	}
	#endregion

	#region Lazy loading
	InputManager GetInputHandler(){
		inputHandler = inputHandler ?? GetComponent<InputManager>();
		Debug.Assert(inputHandler != null, "Missing InputManager component on " + gameObject.name);
		return inputHandler;
	}
	#endregion
}
