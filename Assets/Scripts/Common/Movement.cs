﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(InputManager))]
public class Movement : MonoBehaviour {

	[SerializeField] float speed = 1;

	Rigidbody rigidBody;
	bool isWalking =  false;
	Vector3 direction = Vector3.zero;

	InputManager inputHandler;

	//For injection
	public void Initialize(InputManager input){
		this.inputHandler = input;
	}

	#region Unity lifecycle
	void FixedUpdate(){
		if(GetInputHandler() != null && GetInputHandler().IsInputAllowed()){
			if(GetInputData().xAxis > 0){
				MoveRight();
			}
			else if(GetInputData().xAxis < 0){
				MoveLeft();
			}
			else{
				SetToIdle();
			}
		}
	}
	#endregion

	InputData GetInputData(){
		return inputHandler.GetInputData();
	}

	#region Actions

	void MoveLeft(){
		SetIsWalking(true);
		Rotate();

		direction = new Vector3(GetInputData().xAxis*speed, GetRigidBody().velocity.y,direction.z);
		GetRigidBody().velocity = Vector3.Lerp(direction, direction.normalized * speed, Time.deltaTime * 1);
	}

	void MoveRight(){
		SetIsWalking(true);
		Rotate();

		direction = new Vector3(GetInputData().xAxis*speed, GetRigidBody().velocity.y,direction.z);
		GetRigidBody().velocity = Vector3.Lerp(direction, direction.normalized * speed, Time.deltaTime * 1);
	}

	public void SetToIdle(){
		SetIsWalking(false);
		if(GetRigidBody().velocity.y == 0){
			GetRigidBody().velocity = new Vector3(0/*GetRigidBody().velocity.x*/, 0/*GetRigidBody().velocity.y*/, GetRigidBody().velocity.z);
		}
	}

	void Rotate(){
		Quaternion rotation = transform.rotation;
		rotation = Quaternion.Euler(0, (GetInputData().xAxis > 0) ?90 :-90, 0);
		transform.rotation = rotation;
	}
	#endregion

	#region Setter Getter
	void SetIsWalking(bool isWalking){
		this.isWalking = isWalking;
	}
		
	public bool GetIsWalking(){
		return isWalking;
	}
	#endregion

	#region Lazy loading
	Rigidbody GetRigidBody(){
		rigidBody = rigidBody ?? GetComponent<Rigidbody>();
		Debug.Assert(rigidBody != null, "Missing Rigidbody component on " + gameObject.name);
		return rigidBody;
	}

	InputManager GetInputHandler(){
		inputHandler = inputHandler ?? GetComponent<InputManager>();
		Debug.Assert(inputHandler != null, "Missing InputManager component on " + gameObject.name);
		return inputHandler;
	}
	#endregion
}
