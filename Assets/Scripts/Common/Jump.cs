﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

[RequireComponent(typeof(InputManager))]
[RequireComponent(typeof(Rigidbody))]
public class Jump : MonoBehaviour {

	[SerializeField] float jumpMultiplier = 7;
	[SerializeField] bool canFly = false;

	bool isJumping = false;
	bool isFlying = false;
	Vector3 direction = Vector3.zero;

	InputManager inputHandler;
	Rigidbody rigidBody;

	//For injection
	public void Initialize(InputManager input){
		this.inputHandler = input;
	}

	#region Unity lifecycle
	void Update () {
		if(GetInputHandler() != null && GetInputHandler().IsInputAllowed()){
			if(GetInputHandler().GetInputData().space == ButtonState.ON_DOWN){
				DoJump();
			}
		}
		FallToGround();
	}
	#endregion

	void DoJump(){
		if(GetIsJumping() == false){
			SetIsJumping(true);
			GetRigidBody().velocity = Vector3.Lerp(direction, direction.normalized, Time.deltaTime * 1);
			direction = new Vector3(GetInputHandler().GetInputData().xAxis, jumpMultiplier, 0);
		}
		else if(CanFly()){
			SetIsFlying(true);
			GetRigidBody().velocity = Vector3.Lerp(direction, direction.normalized, Time.deltaTime * 1);
			direction = new Vector3(GetInputHandler().GetInputData().xAxis, jumpMultiplier/4, 0);
		}
	}

	//Temporary groundChecker
	void FallToGround(){
		if(GetRigidBody().velocity.y == 0){
			SetIsJumping(false);
			SetIsFlying(false);
		}
	}

	#region Setter Getter
	public void SetIsJumping(bool isJumping){
		this.isJumping = isJumping;
	}

	public bool GetIsJumping(){
		return isJumping;
	}

	public void SetIsFlying(bool isFlying){
		this.isFlying = isFlying;
	}

	public bool GetIsFlying(){
		return isFlying;
	}

	public void SetCanFly(bool canFly){
		this.canFly = canFly;
	}

	public bool CanFly(){
		return canFly;
	}
	#endregion

	#region Lazy loading
	Rigidbody GetRigidBody(){
		rigidBody = rigidBody ?? GetComponent<Rigidbody>();
		Debug.Assert(rigidBody != null, "Missing Rigidbody component on " + gameObject.name);
		return rigidBody;
	}

	InputManager GetInputHandler(){
		inputHandler = inputHandler ?? GetComponent<InputManager>();
		Debug.Assert(inputHandler != null, "Missing InputManager component on " + gameObject.name);
		return inputHandler;
	}
	#endregion
}
