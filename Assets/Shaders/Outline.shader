﻿Shader "Custom/Outline"
{
    Properties {
		_Tint ("Tint", Color) = (1, 1, 1, 1)
		_MainTex ("Texture", 2D) = "white" {}
		_OutlineSize ("_OutlineSize", Range(0,0.1)) = 0.5
        _OutlineColor ("_OutlineColor", Color) = (1, 1, 1, 1)
	}

	SubShader {
	     Tags { "RenderType"="Opaque" }

		Pass {
            Cull Front
 
            CGPROGRAM
 
            #pragma vertex MyVertexProgram
            #pragma fragment MyFragmentProgram
            #include "UnityCG.cginc"

            float _OutlineSize;
            float4 _OutlineColor;

            struct VertexData {
                float4 position : SV_POSITION;
            };
 
            float4 MyVertexProgram(appdata_base ab) : SV_POSITION {
                VertexData v;
                v.position = mul(UNITY_MATRIX_MVP, ab.vertex);
                float3 normal = mul((float3x3) UNITY_MATRIX_MV, ab.normal);
                normal.x *= UNITY_MATRIX_P[0][0];
                normal.y *= UNITY_MATRIX_P[1][1];
                v.position.xy += normal.xy * _OutlineSize;
                return v.position;
            }
 
            half4 MyFragmentProgram(VertexData v) : COLOR {
                return _OutlineColor;
            }
 
            ENDCG
        }

		Pass {

			CGPROGRAM
			#pragma vertex MyVertexProgram
			#pragma fragment MyFragmentProgram

			#include "UnityCG.cginc"

			float4 _Tint;
			sampler2D _MainTex;
			float4 _MainTex_ST;

			struct VertexData{
				float4 position : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct Interpolators {
				float4 position : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			Interpolators MyVertexProgram (VertexData v) {
				Interpolators i;
				i.position = mul(UNITY_MATRIX_MVP, v.position);
				i.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return i;
			}

			float4 MyFragmentProgram (Interpolators i) : SV_TARGET{
				return tex2D(_MainTex, i.uv) * _Tint;
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
